import 'zone.js/dist/zone-mix'
import 'reflect-metadata'
import '../polyfills'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

// Forms modules
import { FormsModule } from '@angular/forms'

// Material modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatTooltipModule } from '@angular/material/tooltip'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatGridListModule } from '@angular/material/grid-list'

const MATERIAL_MODULES = [
  BrowserAnimationsModule,
  MatButtonModule,
  MatIconModule,
  MatTooltipModule,
  MatToolbarModule,
  MatGridListModule
]

// Toastr modules
// https://www.npmjs.com/package/ngx-toastr - to configure
import { ToastrModule } from 'ngx-toastr'

// Http client modules
import { HttpClientModule, HttpClient } from '@angular/common/http'

// Routing modules
import { AppRoutingModule } from './app-routing.module'

// Angular translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

// Electron services
import { ElectronService } from './providers/electron.service'

// App components
import { AppComponent } from './app.component'
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    MATERIAL_MODULES,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot({
      timeOut: 3000
    })
  ],
  providers: [ElectronService],
  bootstrap: [AppComponent]
})
export class AppModule { }
